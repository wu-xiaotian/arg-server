const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const drug =  sequelize.define("drug",{
  drug_id:DataTypes.NUMBER,
  producename:DataTypes.CHAR,
  brand_id:DataTypes.NUMBER,
  manufacturers_id:DataTypes.NUMBER,
  content:DataTypes.NUMBER,
  poisonousness_id:DataTypes.NUMBER,
  dosage:DataTypes.CHAR,
  ingredient:DataTypes.CHAR,
  cardno:DataTypes.CHAR,
  loginid:DataTypes.CHAR,
  drugname:DataTypes.CHAR,
  activecontent:DataTypes.DECIMAL,
  cureobject:DataTypes.CHAR,
  approvalnumber:DataTypes.CHAR
});

module.exports = drug