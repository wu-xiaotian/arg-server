const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const Biology =  sequelize.define("Biology",{
  biology_id:DataTypes.NUMBER,
  bname_id:DataTypes.TEXT,
  category_id:DataTypes.NUMBER
});

module.exports = Biology