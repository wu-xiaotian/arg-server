const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const historyselect =  sequelize.define("historyselect",{
  user_id:DataTypes.NUMBER,
  key:DataTypes.TEXT,
});

module.exports = historyselect;