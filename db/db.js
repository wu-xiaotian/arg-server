const { Sequelize } = require('sequelize');


// 前三个参数是   数据库 、  用户名   、 密码
const sequelize = new Sequelize('', '', '', {
    host: '',    //主机地址  例如： 192.168.0.1  或者  localhost
    dialect: 'mysql',  //数据库类型，例如 mysql
    define: {
      freezeTableName: true
    }
  });

module.exports = sequelize;