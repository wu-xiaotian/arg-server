const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const illness =  sequelize.define("illness",{
  illname:DataTypes.CHAR,
  symptom:DataTypes.TEXT,
  cause:DataTypes.TEXT,
  spread:DataTypes.TEXT,
  hostplant:DataTypes.CHAR,
  characteristic:DataTypes.TEXT,
  feature:DataTypes.TEXT,
  habit:DataTypes.TEXT,
  method:DataTypes.TEXT,
  passing:DataTypes.CHAR,
});

module.exports = illness