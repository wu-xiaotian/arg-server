const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const userdb =  sequelize.define("userdb",{
  username:DataTypes.CHAR,
  userpassword:DataTypes.CHAR,
  role:DataTypes.CHAR
});

module.exports = userdb