const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const userBiology =  sequelize.define("userBiology",{
  user_biology_id:DataTypes.NUMBER,
  user_id:DataTypes.NUMBER,
  bname_id:DataTypes.NUMBER,
  feeding:DataTypes.NUMBER
});

module.exports = userBiology