const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const articles =  sequelize.define("articles",{
  title:DataTypes.CHAR,
  content:DataTypes.TEXT,
  passing:DataTypes.NUMBER,
});

module.exports = articles