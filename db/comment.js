const { DataTypes } = require('sequelize');
const sequelize = require('./db')

const comment =  sequelize.define("comment",{
  comment_id:DataTypes.NUMBER,
  drug_id:DataTypes.NUMBER,
  content:DataTypes.CHAR,
  userid:DataTypes.NUMBER,
  username:DataTypes.CHAR,
  like:DataTypes.NUMBER,

});

module.exports = comment