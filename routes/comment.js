const express = require('express');
const router = express.Router();
const server= require('../server/comment')

//http://localhost:0908/api/comment/getDrugComments  查找推荐药品的评论  参数drugId
router.get('/getDrugComments', async(req, res) => {
    try {
        const result =await server.getAllComments(req.query.drugId);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

//http://localhost:0908/api/comment/addComment  添加评论  参数  userId   drugId  content  username
router.post('/addComment', async(req, res) => {
    try {
        const result =await server.AddComment(req.body.userId,req.body.drugId,req.body.content,req.body.username);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})

//http://localhost:0908/api/comment/deleteComment  删除评论  参数  content   
router.post('/deleteComment', async(req, res) => {
    try {
        const result =await server.deleteComment(req.body.content);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})



router.get('/getPageComments', async(req, res) => {
    try {
        const result =await server.getPageComments(req.query.pageNo);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

router.delete('/deleteCommentById', async(req, res) => {
    try {
        const result =await server.deleteCommentById(req.body.comment_id);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

router.get('/selectCommentByContent', async(req, res) => {
    try {
        const result =await server.selectCommentByContent(req.query.content);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

module.exports = router;