const express = require('express');
const router = express.Router();
const server= require('../server/drug')

//http://localhost:0908/api/drug/recommendDrugs   查看推荐的药品  参数 illnessName
router.get('/recommendDrugs', async(req, res) => {
    try {
        const result =await server.recommendDrugs(req.query.illnessName);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})

router.get('/getAllDrugs', async(req, res) => {
    try {
        const result =await server.getALLDrugs(req.query.pageNo);
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }
    
})

router.post('/deleteDrugById', async(req, res) => {
    try {
        const result =await server.deleteDrugById(req.body.drug_id);
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }
    
})

router.post('/insertDrug', async(req, res) => {
    try {
        const result =await server.insertDrug(req.body.producename,req.body.brand_id,req.body.manufacturers_id,req.body.content,req.body.poisonousness_id,req.body.dosage,req.body.ingredient,req.body.cardno,req.body.loginid,req.body.drugname,req.body.activecontent,req.body.cureobject,req.body.approvalnumber);
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }
    
})

router.get('/selectDrugById', async(req, res) => {
    try {
        const result =await server.selectDrugById(req.query.drug_id);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})

router.post('/updateDrug', async(req, res) => {
    try {
        const result =await server.updateDrug(req.body.drug_id,req.body.producename,req.body.brand_id,req.body.manufacturers_id,req.body.content,req.body.poisonousness_id,req.body.dosage,req.body.ingredient,req.body.cardno,req.body.loginid,req.body.drugname,req.body.activecontent,req.body.cureobject,req.body.approvalnumber);
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }
    
})

router.get('/selectDrugByName', async(req, res) => {
    try {
        const result =await server.selectDrugByName(req.query.producename);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})
module.exports = router;