const express = require('express');
const router = express.Router();
const server = require('../server/illness')

router.delete('/deleteIllness', async (req, res) => {
    try {
        const result = await server.deleteIllness(req.query.illness_id);
        res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }

})

router.post('/updateIllness', async (req, res) => {
    try {
        const result = await server.updateIllness(req.body.illness_id,req.body.illname,req.body.class,req.body.symptom,req.body.cause,req.body.spread,req.body.hostplant,req.body.characteristic,req.body.feature,req.body.habit,req.body.method,req.body.passing);
        res.send(result)
    } catch (error) {
        console.log(error);
        res.send(false)
    }

})

router.post('/insertIllness', async (req, res) => {
    try {
        const result = await server.insertIllness(req.body.illname,req.body.class,req.body.symptom,req.body.cause,req.body.spread,req.body.hostplant,req.body.characteristic,req.body.feature,req.body.habit,req.body.method,req.body.passing);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.get('/allIllness', async (req, res) => {
    try {
        const result = await server.getAllIllness(req.query.pageNo);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.get('/', async (req, res) => {
    try {
        const result = await server.getIllnessById(req.query.illness_id);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})
router.get('/illnessList', async (req, res) => {
    try {
        const result = await server.getIllness(req.query.key);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.get('/selectIllnessByName', async (req, res) => {
    try {
        const result = await server.selectIllnessByName(req.query.illname);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})
module.exports = router;