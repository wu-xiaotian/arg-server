const express = require('express');
const router = express.Router();
const server= require('../server/articles')
router.get('/', async(req, res) => {
    try {
        const result =await server.getArticles();
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})


router.get('/details', async(req, res) => {
    try {
        const result =await server.getArticleById(req.query.articleId);
    res.send(result)
    } catch (error) {
        res.send({
            code:500,
            msg:"文章详情获取失败!"
        })
    }
    
})

router.post('/insertArticle', async(req, res) => {
    try {
        const result =await server.insertArticle(req.body.title,req.body.content);
        
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send({
            code:500,
            msg:"文章添加失败!"
        })
    }
    
})

router.get('/getPageArticles', async(req, res) => {
    try {
        const result =await server.getPageArticles(req.query.pageNo);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

router.delete('/deleteArticleById', async(req, res) => {
    try {
        const result =await server.deleteArticleById(req.body.article_id);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

router.post('/updateArticleById', async(req, res) => {
    try {
        const result =await server.updateArticle(req.body.article_id,req.body.title,req.body.content);
        
    res.send(result)
    } catch (error) {
        console.log(error);
        res.send({
            code:500,
            msg:"文章修改失败!"
        })
    }
    
})

router.get('/selectAritlceByTitle', async(req, res) => {
    try {
        const result =await server.selectAritlceByTitle(req.query.title);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})

module.exports = router;