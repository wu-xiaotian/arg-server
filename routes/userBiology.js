const express = require('express');
const router = express.Router();
const server= require('../server/userBiology');

// http://localhost:0908/api/userBiology/userAllBiology   获取id用户的生物  参数userId
router.post('/userAllBiology', async(req, res) => {
    try {
        const result =await server.getUserAllBiology(req.body.userId);
    res.send(result)
    } catch (error) {
        res.send(false)
    }

    
})

// http://localhost:0908/api/userBiology/addUserBiology   添加id用户的生物  参数userId，bname
router.post('/addUserBiology', async(req, res) => {
    try {
        const result =await server.addUserBiology(req.body.userId,req.body.bname);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})
// http://localhost:0908/api/userBiology/deleteUserBiology   删除养殖的生物
router.post('/deleteUserBiology', async(req, res) => {
    try {
        const result =await server.deleteUserBiology(req.body.userId,req.body.bname);
    res.send(result)
    } catch (error) {
        res.send(false)
    }
    
})

// http://localhost:0908/api/userBiology/liveUserBiology   激活养殖的生物
router.post('/liveUserBiology', async(req, res) => {
    try {
        const result =await server.liveUserBiology(req.body.userId,req.body.bname);
    res.send(result)
    } catch (error) {
        res.send(error)
    }
    
})

module.exports = router;