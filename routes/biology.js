const express = require('express');
const router = express.Router();
const server = require('../server/biology')
router.get('/allBiology', async (req, res) => {
    try {
        const result = await server.getAllBiology();
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.get('/getPagesBiology', async (req, res) => {
    try {
        const result = await server.getPagesBiology(req.query.pageNo);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.delete('/deleteBiologyById', async (req, res) => {
    try {
        const result = await server.deleteBiologyById(req.query.biology_id);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.post('/insertBiology', async (req, res) => {
    try {
        const result = await server.insertBiology(req.body.bname);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.post('/updateBiology', async (req, res) => {
    try {
        const result = await server.updateBiology(req.body.biology_id,req.body.bname);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})

router.get('/selectBiologyByName', async (req, res) => {
    try {
        const result = await server.selectBiologyByName(req.query.bname);
        res.send(result)
    } catch (error) {
        res.send(false)
    }

})
module.exports = router;