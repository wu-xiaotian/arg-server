const express = require('express');
const router = express.Router();
const server= require('../server/historyselect')
router.get('/allSelects', async(req, res) => {
    try {
        const result =await server.getALLHistorySelect(req.query.user_id);
    res.send(result)
    } catch (error) {
        res.send({
            code:500,
            msg:"历史搜索获取失败!"
        });
    }
    
})

router.get('/insertSelects', async(req, res) => {
    try {
        const result =await server.insertHistorySelect(req.query.user_id,req.query.key);
    res.send(result)
    } catch (error) {
        res.send(false);
    }
    
})

module.exports = router;