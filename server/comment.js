const comment = require('../db/comment')
const { QueryTypes } = require('sequelize');



exports.getAllComments= async function(drugId){
    const result = await comment.sequelize.query(
        `select * from comment  where comment.drug_id = ${drugId} order by comment_id desc`,
        {type:QueryTypes.SELECT}
    );
    return result;
}

exports.AddComment= async function(userId,drugId,content,username){
    const result = await comment.sequelize.query(
        `insert into comment(drug_id,content,userid,username) values(${drugId},'${content}',${userId},'${username}')`
    );
    return result;
}

exports.deleteComment= async function(content){
    const result = await comment.sequelize.query(
        `delete  from comment where content = "${content}"; `
    );
    return result;
}

exports.getPageComments = async function (pageNo) {
    let result;
    let counts;
    if (pageNo>=0) {
        result = await comment.sequelize.query(`select * from comment limit  ${(pageNo-1)*10}, 10;`, { type: QueryTypes.SELECT });
        counts = await comment.sequelize.query("select count(*) as counts from comment ;", { type: QueryTypes.SELECT });
    } else if (pageNo<0) {
        return false;

    }
    return {
        result,
        counts
    };
}


exports.deleteCommentById= async function(comment_id){
    const result = await comment.sequelize.query(
        `delete  from comment where comment_id = "${comment_id}"; `
    );
    return result;
}

exports.selectCommentByContent= async function(content){
    const result = await comment.sequelize.query(
        `select * from comment where content = "${content}"; `, { type: QueryTypes.SELECT }
    );
    return result;
}