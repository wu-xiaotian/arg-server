const biology = require('../db/biology')
const { QueryTypes } = require('sequelize');

exports.getAllBiology = async function () {
    let biologys = await biology.sequelize.query(`select bname from biology;`,{ type: QueryTypes.SELECT });

    return biologys;
}

exports.getPagesBiology = async function (pageNo) {
    let biologys = await biology.sequelize.query(`select * from biology limit  ${(pageNo-1)*10}, 10;`,{ type: QueryTypes.SELECT });
    let counts = await biology.sequelize.query(`select count(*) as counts from biology;`,{ type: QueryTypes.SELECT });

    return {
        biologys,
        counts
    }
}

exports.deleteBiologyById = async function (biology_id) {
    let result = await biology.sequelize.query(`delete from biology where biology_id=${biology_id}`);

    return result;
}

exports.insertBiology = async function (bname) {
    let result = await biology.sequelize.query(`insert biology(bname) values('${bname}')`,{ type: QueryTypes.SELECT });

    return result;
}

exports.updateBiology = async function (biology_id,bname) {
    let result = await biology.sequelize.query(`update biology set bname = '${bname}' where biology_id = '${biology_id}'`,{ type: QueryTypes.SELECT });

    return result;
}

exports.selectBiologyByName = async function (bname) {
    let result = await biology.sequelize.query(`select * from biology where bname='${bname}'`,{ type: QueryTypes.SELECT });

    return result;
}