const historyselect = require('../db/historyselect')
const { QueryTypes } = require('sequelize');
exports.getALLHistorySelect = async function (user_id) {
    let results= await historyselect.sequelize.query(`select COUNT(all_history_select.${`key`}) as count, all_history_select.${`key`} from (select * from historyselect where user_id = ${user_id}  order by id desc limit 100 ) as all_history_select GROUP BY all_history_select.key;`,
    { type: QueryTypes.SELECT });
    
    return results;
}

exports.insertHistorySelect = async function (user_id,key) {
    let results= await historyselect.create({ user_id: user_id, key: key });
    
    return results;
}