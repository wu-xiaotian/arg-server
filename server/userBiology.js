const userBiology = require('../db/userBiology')
const { QueryTypes } = require('sequelize');

exports.getUserAllBiology= async function(userId){
    const users = await userBiology.sequelize.query(
        `select * from biology inner join user_biology on biology.biology_id = user_biology.bname_id and user_biology.user_id = ${userId}`,
        { type: QueryTypes.SELECT }
    );
    return users;
}

exports.addUserBiology= async function(userId,bname){
    const users = await userBiology.sequelize.query(
        `insert into user_biology(user_id,bname_id,feeding) values('${userId}', (select biology_id from biology where bname ='${bname}'),1)`
    );
    return users;
}

exports.deleteUserBiology= async function(userId,bname){
    const users = await userBiology.sequelize.query(
        `update user_biology set feeding = 0 where user_id = ${userId} and bname_id = (select biology_id from biology where bname = '${bname}')`
    );
    return users;
}

exports.liveUserBiology= async function(userId,bname){
    const users = await userBiology.sequelize.query(
        `update user_biology set feeding = 1 where user_id = ${userId} and bname_id = (select biology_id from biology where bname = '${bname}')`
    );
    return users;
}