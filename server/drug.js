const drug = require('../db/drug')
const { QueryTypes } = require('sequelize');

exports.recommendDrugs= async function(illnessName){
    let result = await drug.sequelize.query(
        `select * from drug  where cureobject like '%${illnessName}%'`,
        {type:QueryTypes.SELECT}
    );
    return result;
}

exports.getALLDrugs= async function(pageNo){
    let results = await drug.sequelize.query(
        `select * from drug  limit  ${(pageNo-1)*10}, 10;`,
        {type:QueryTypes.SELECT}
    );
    let counts = await drug.sequelize.query("select count(*) as counts from drug ;", { type: QueryTypes.SELECT });
    return {
        results,
        counts
    };
}

exports.deleteDrugById= async function(drug_id){
    let results = await drug.sequelize.query(
        `delete from drug  where drug_id = '${drug_id}'`,
        {type:QueryTypes.SELECT}
    );
    return results
        
}

exports.insertDrug= async function(producename,
    brand_id,
    manufacturers_id,
    content,
    poisonousness_id,
    dosage,
    ingredient,
    cardno,
    loginid,
    drugname,
    activecontent,
    cureobject,
    approvalnumber){
    let results = await drug.sequelize.query(
        `insert drug(producename,brand_id,manufacturers_id,content,poisonousness_id,dosage,ingredient,cardno,loginid,drugname,activecontent,cureobject,approvalnumber) values('${producename}','${brand_id}','${manufacturers_id}','${content}','${poisonousness_id}','${dosage}','${ingredient}','${cardno}','${loginid}','${drugname}','${activecontent}','${cureobject}','${approvalnumber}') ; `);
    return results
        
}

exports.selectDrugById= async function(drug_id){
    let result = await drug.sequelize.query(
        `select * from drug  where drug_id = '${drug_id}%'`,
        {type:QueryTypes.SELECT}
    );
    return result;
}

exports.updateDrug= async function(
    drug_id,
    producename,
    brand_id,
    manufacturers_id,
    content,
    poisonousness_id,
    dosage,
    ingredient,
    cardno,
    loginid,
    drugname,
    activecontent,
    cureobject,
    approvalnumber){
    let results = await drug.sequelize.query(
        `update drug set producename='${producename}',brand_id='${brand_id}',manufacturers_id='${manufacturers_id}',content='${content}',poisonousness_id='${poisonousness_id}',dosage='${dosage}',ingredient='${ingredient}',cardno='${cardno}',loginid='${loginid}',drugname='${drugname}',activecontent='${activecontent}',cureobject='${cureobject}',approvalnumber='${approvalnumber}' where drug_id = '${drug_id}' ; `);
    return results
        
}

exports.selectDrugByName= async function(producename){
    let result = await drug.sequelize.query(
        `select * from drug  where producename = '${producename}'`,
        {type:QueryTypes.SELECT}
    );
    return result;
}