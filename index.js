const express = require('express')
const app = express()
const cors = require('cors')
const userdbRoute = require('./routes/userdb');
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(userdbRoute)
app.use(cors())                                                 //处理跨域
app.use("/api/userdb",require('./routes/userdb'))
app.use("/api/illness",require('./routes/illness'))
app.use("/api/articles",require('./routes/articles'))
app.use("/api/historyselect",require('./routes/historyselect'))
app.use("/api/userBiology",require('./routes/userBiology'))
app.use("/api/drug",require('./routes/drug'))
app.use("/api/comment",require('./routes/comment'))
app.use("/api/biology",require('./routes/biology'))


const port = 0908
app.listen(port);