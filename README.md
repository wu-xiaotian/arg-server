# Express农业疾病后端服务

#### 介绍

农业疾病的后端服务

#### 软件架构

使用Express和Sequelize框架，为前台提供服务。

#### 安装教程

1. npm i;
2. node index

#### 使用说明

1. db文件夹是数据库文件夹，里面db.js用来配置数据库连接，其他文件则对应着数据库的每一张表。
2. routes文件夹是路由管理，当请求进来时可以分配不同的处理函数。
3. server文件夹是不同的处理函数，用来增删改查

#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request

#### 特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5. Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
